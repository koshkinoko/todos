import {processError} from './error-helper';

export function doGet(url, param) {
    return doFetch(url, 'GET', param);
}

export function doPost(url, body) {
    return doFetch(url, 'POST', null, body);
}

export function doPut(url, param, body) {
    return doFetch(url, 'PUT', param, body);
}

export function doDelete(url, param) {
    return doFetch(url, 'DELETE', param);
}

function doFetch(url, type, param, body) {
    const res = fetch('http://localhost:3000/api/v1/' + url + (param ? '/' + param : ''), {
        method: type,
        credentials: 'include',
        headers: {"Content-Type": "application/json"},
        body: body
    }).then(response => {
        if (response.status >= 400) {
            throw response;
        }
        return response.json();
    }).catch((error) => {
        const status = error.status;
        if(!status){
            return {isError: false};
        }
        return error.json().then(body => {
            return {isError: true, error: processError(status, body)};
        }).catch(() => {
            return {isError: true, error: processError(status)};
        })
    });
    return res;
}