import {SESSION_EXPIRED} from '../actions/action-types';
import {sessionExpired} from '../actions/login-actions';

export function processError(status, body) {
    switch (status) {
        case 400:
            return {
                showModal: true,
                message: body.message
            };
        case 401:
            return {
                action: sessionExpired(),
                showModal: true,
                message: 'Session has expired, please sign in again/'
            };
        case 403:
            return {
                showModal: true,
                message: 'You don\'t have enough rights for this action/'
            };
        case 404:
            return {
                showModal: true,
                message: 'Somebody has updated the data. Please, reload page.'
            };
        default:
            return {
                showModal: true,
                message: 'Something went wrong. Please, try later.'
            };
    }
}

export function required(value) {
    if(!value) {
        return 'Required';
    }
}