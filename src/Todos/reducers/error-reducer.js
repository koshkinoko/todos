import {CLOSE_MODAL} from '../actions/action-types';

const initialState = {};

export const errorReducer = (state = initialState, action) => {
    if (action.type.includes('_ERROR')) {
        return action.payload;
    } else if(action.type===CLOSE_MODAL) {
        return initialState;
    }
    return state;
};