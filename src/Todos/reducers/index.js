import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import {errorReducer} from './error-reducer';
import {todosReducer} from './todos-reducer';
import {userReducer} from './user-reducer';
import {usersReducer} from './users-reducer';

export const rootReducer = combineReducers({
    form: formReducer, // this is the form reducer
    user: userReducer,
    todos: todosReducer,
    users: usersReducer,
    error: errorReducer
});