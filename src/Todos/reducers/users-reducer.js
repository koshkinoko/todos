import {USERS} from '../actions/action-types';
import {ERROR, FETCH, SUCCESS} from '../actions/action-postfixes';
import {STATUS_FETCH_ERROR, STATUS_FETCH_SUCCESS, STATUS_FETCHING, STATUS_IDLE} from '../consts';

const initialState = {status: STATUS_IDLE, users: []};

export const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case USERS + FETCH:
            return Object.assign(state, {status: STATUS_FETCHING});
        case USERS + SUCCESS:
            return {status: STATUS_FETCH_SUCCESS, users: action.payload};
        case USERS + ERROR:
            return Object.assign(state, {status: STATUS_FETCH_ERROR});

        default:
            return state;
    }
};

