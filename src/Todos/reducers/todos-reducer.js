import {CREATE_TODO, TODOLIST} from '../actions/action-types';
import {ERROR, FETCH, SUCCESS} from '../actions/action-postfixes';
import {STATUS_FETCH_ERROR, STATUS_FETCH_SUCCESS, STATUS_FETCHING, STATUS_IDLE} from '../consts';

const initialState = {status: STATUS_IDLE, todolist: []};

export const todosReducer = (state = initialState, action) => {
    switch (action.type) {
        case TODOLIST + FETCH:
            return Object.assign(state, {status: STATUS_FETCHING});
        case TODOLIST + SUCCESS:
            return {status: STATUS_FETCH_SUCCESS, todolist: action.payload};
        case TODOLIST + ERROR:
            return Object.assign(state, {status: STATUS_FETCH_ERROR});
        case CREATE_TODO + SUCCESS:
            return {...state, todolist: [...state.todolist, action.payload]};

        default:
            return state;
    }
};