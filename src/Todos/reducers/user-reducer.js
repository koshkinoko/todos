import {CHECK_SESSION, LOGIN, LOGOUT, SESSION_EXPIRED} from '../actions/action-types';
import {ERROR, FETCH, SUCCESS} from '../actions/action-postfixes';
import {STATUS_FETCH_ERROR, STATUS_FETCH_SUCCESS, STATUS_FETCHING, STATUS_IDLE} from '../consts';

const initialState = {status: STATUS_IDLE, loggedIn: null};

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN + FETCH:
            return Object.assign(state, {status: STATUS_FETCHING});
        case LOGIN + SUCCESS:
            return {status: STATUS_FETCH_SUCCESS, loggedIn: true, name: action.payload.name, role: action.payload.role};
        case LOGIN + ERROR:
            return Object.assign(state, {status: STATUS_FETCH_ERROR});
        case LOGOUT + FETCH:
            return Object.assign(state, {status: STATUS_FETCHING});
        case LOGOUT + SUCCESS:
            return {status: STATUS_IDLE, loggedIn: false};
        case LOGOUT + ERROR:
            return Object.assign(state, {status: STATUS_FETCH_ERROR});
        case CHECK_SESSION + FETCH:
            return Object.assign(state, {status: STATUS_FETCHING});
        case SESSION_EXPIRED:
            return {status: STATUS_FETCH_ERROR, loggedIn: false};
        case CHECK_SESSION + SUCCESS:
            return {status: STATUS_FETCH_ERROR, loggedIn: true, name: action.payload.name, role: action.payload.role};

        default:
            return state;
    }
};

