export const homePath="/";
export const todosPath="/todos";
export const usersPath="/users";
export const loginPath="/login";

export const privatePaths=[todosPath, homePath];
export const superuserPaths=[usersPath];
export const unauthorizedPaths=[loginPath];