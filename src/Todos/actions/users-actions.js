import {USERS} from './action-types';
import {ERROR, FETCH, SUCCESS} from './action-postfixes';

export const getUserList = () => ({
    type: USERS + FETCH
});

export const getUserListError = error => ({
    type: USERS + ERROR,
    payload: error
});

export const getUserListSuccess = (payload) => ({
    type: USERS + SUCCESS,
    payload
});
