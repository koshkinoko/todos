import {CHECK_SESSION, LOGIN, LOGOUT, SESSION_EXPIRED} from './action-types';
import {ERROR, FETCH, SUCCESS} from './action-postfixes';

export const login = (payload, meta) => ({
    type: LOGIN + FETCH,
    payload,
    meta
});

export const loginError = error => ({
    type: LOGIN + ERROR,
    payload: error
});

export const loginSuccess = (payload) => ({
    type: LOGIN + SUCCESS,
    payload
});

export const logout = () => ({
    type: LOGOUT + FETCH,
});

export const logoutError = error => ({
    type: LOGOUT + ERROR,
    payload: error
});

export const logoutSuccess = () => ({
    type: LOGOUT + SUCCESS
});

export const checkSession = () => ({type: CHECK_SESSION + FETCH});

export const sessionExpired = () => ({type: SESSION_EXPIRED});

export const checkSessionError = error => ({
    type: CHECK_SESSION + ERROR,
    payload: error
});

export const checkSessionSuccess = (payload) => ({
    type: CHECK_SESSION + SUCCESS,
    payload
});