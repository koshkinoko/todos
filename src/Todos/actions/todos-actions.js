import {CREATE_TODO, DELETE_TODO, TODOLIST, UPDATE_TODO} from './action-types';
import {ERROR, FETCH, SUCCESS} from './action-postfixes';

export const getTodolist = () => ({
    type: TODOLIST + FETCH
});

export const getTodolistError = error => ({
    type: TODOLIST + ERROR,
    payload: error
});

export const getTodolistSuccess = (payload) => ({
    type: TODOLIST + SUCCESS,
    payload
});

export const createTodo = (payload) => ({
    type: CREATE_TODO + FETCH,
    payload
});

export const createTodoError = error => ({
    type: CREATE_TODO + ERROR,
    payload: error
});

export const createTodoSuccess = (payload) => ({
    type: CREATE_TODO + SUCCESS,
    payload
});

export const updateTodo = (payload) => ({
    type: UPDATE_TODO + FETCH,
    payload
});

export const updateTodoError = error => ({
    type: UPDATE_TODO + ERROR,
    payload: error
});

export const updateTodoSuccess = () => ({
    type: UPDATE_TODO + SUCCESS
});

export const deleteTodo = (payload) => ({
    type: DELETE_TODO + FETCH,
    payload
});

export const deleteTodoError = error => ({
    type: DELETE_TODO + ERROR,
    payload: error
});

export const deleteTodoSuccess = () => ({
    type: DELETE_TODO + SUCCESS
});

