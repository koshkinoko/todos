export const LOGIN = 'LOGIN';

export const LOGOUT = 'LOGOUT';

export const CHECK_SESSION = 'CHECK_SESSION';

export const SESSION_EXPIRED = 'SESSION_EXPIRED';

export const CLOSE_MODAL = 'CLOSE_MODAL';

export const TODOLIST = 'TODOLIST';

export const CREATE_TODO = 'CREATE_TODO';

export const UPDATE_TODO = 'UPDATE_TODO';

export const DELETE_TODO = 'DELETE_TODO';

export const USERS = 'USERS';

