import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createTodo, getTodolist, updateTodo} from '../actions/todos-actions';
import {getTodos} from '../selectors/todo-selectors';
import Todo from './Todo';


class Todos extends Component {
    constructor(props) {
        super(props);

        this.handleCreateNewTodoClick = this.handleCreateNewTodoClick.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleCancelClick = this.handleCancelClick.bind(this);
        this.handleSaveChangesClick = this.handleSaveChangesClick.bind(this);
        this.state = {
            todoEditing: null,
            newTodoCreating: false
        }
    }

    componentDidMount() {
        this.props.getTodos();
    }

    handleEditClick(id) {
        if (!this.state.todoEditing && !this.state.newTodoCreating) {
            this.setState({todoEditing: id});
        }
    }

    handleCancelClick(){
        if(this.state.newTodoCreating) {
            this.setState({newTodoCreating: false});
        } else {
            this.setState({todoEditing: null});
        }
    }


    handleSaveChangesClick(values) {
        if(this.state.newTodoCreating) {
            this.setState({newTodoCreating: false});
            this.props.createTodo(values);
        } else {
            this.setState({todoEditing: null});
            this.props.updateTodo(values);
        }
    }

    handleCreateNewTodoClick(e) {
        e.preventDefault();
        if(!this.state.todoEditing) {
            this.setState({newTodoCreating: true});
        }
    }

    render() {
        const {todos} = this.props;
        const {todoEditing, newTodoCreating} = this.state;

        return (
            <div className="todolist">
                {
                    newTodoCreating &&
                    <Todo todo={{}} isEditing={true}
                          handleSaveChangesClick={this.handleSaveChangesClick}
                          handleCancelClick = {this.handleCancelClick}/>
                }
                <button onClick={this.handleCreateNewTodoClick}>Create new todo</button>
                {todos.map(todo => {
                    return (
                        <Todo todo={todo} isEditing={todo.id === todoEditing }
                              handleEditClick={this.handleEditClick}
                              handleSaveChangesClick={this.handleSaveChangesClick}
                              handleCancelClick = {this.handleCancelClick}
                              key={todo.id}/>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        todos: getTodos(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTodos: () => dispatch(getTodolist()),
        createTodo: (values) => dispatch(createTodo(values)),
        updateTodo: (values) => dispatch(updateTodo(values)),
    }
};

export default connect(
    mapStateToProps, mapDispatchToProps
)(Todos);