import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';
import {Link} from 'react-router-dom';
import {logout} from '../actions/login-actions';
import {homePath, todosPath, usersPath} from '../route-consts';
import {isSuperuser} from '../selectors/user-selectors';
import {getUsername} from '../selectors/user-selectors';


class Navbar extends Component {

    constructor(props) {
        super(props);

        this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }

    handleLogoutClick(e) {
        e.preventDefault();
        this.props.logout();
    }

    render() {
        const {username, isSuperuser} = this.props;
        if(!username){
            return <Redirect to='/login'/>;
        }

        return (
            <div className="navbar" style={{width: "100%", backgroundColor: "yellow"}}>
                <span>{username}</span>
                <Link to={homePath}>Home</Link>
                <Link to={todosPath}>Todos</Link>
                { isSuperuser &&
                    <Link to={usersPath}>Users</Link>
                }
                <button onClick={this.handleLogoutClick}>Logout</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        username: getUsername(state),
        isSuperuser: isSuperuser(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(logout())
    }
};

export default connect(
    mapStateToProps, mapDispatchToProps
)(Navbar);