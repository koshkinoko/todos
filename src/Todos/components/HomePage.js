import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getUsername} from '../selectors/user-selectors';

class HomePage extends Component {

    render() {
        const {username} = this.props;

        return (
            <div className="container">
                <p>Hello, {username}</p>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        username: getUsername(state)
    }
};

export default connect(
    mapStateToProps, null
)(HomePage);