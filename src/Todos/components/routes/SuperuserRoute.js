import React from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';
import {homePath} from '../../route-consts';
import {isSuperuser} from '../../selectors/user-selectors';
import {LoggedInPage} from '../LoggedInPage';

const SuperuserRoute = ({component: Component, ...props}) => {
    const {isSuperuser} = props;
    return (
        <Route {...props} render={props => (
            isSuperuser
                ? <LoggedInPage><Component {...props} /></LoggedInPage>
                : <Redirect to={{pathname: homePath, state: {from: props.location}}}/>
        )}/>
    );
};

const mapStateToProps = (state) => {
    return {
        isSuperuser: isSuperuser(state)
    }
};

export default connect(
    mapStateToProps
)(SuperuserRoute);