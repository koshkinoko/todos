import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, Route} from 'react-router-dom';
import {checkSession} from '../../actions/login-actions';
import {homePath, loginPath, privatePaths, superuserPaths, unauthorizedPaths} from '../../route-consts';
import {getLoggedIn} from '../../selectors/user-selectors';
import {Loading} from '../Loading';
import {LoggedInPage} from '../LoggedInPage';
import SuperuserRoute from './SuperuserRoute';

class AppRoute extends Component {

    componentDidMount() {
        if (this.props.loggedIn == null) {
            this.props.checkSession();
        }
    }

    render() {
        const {loggedIn, path} = this.props;
        const {component: Component, ...rest} = this.props;
        if (loggedIn == null) {
            return <Loading/>;
        } else if (!loggedIn) {
            if (!unauthorizedPaths.includes(path)) {
                return <Redirect to={loginPath}/>;
            }
            return <Component {...this.props} />;
        } else {
            if (superuserPaths.includes(path)) {
                return <SuperuserRoute {...this.props}/>;
            } else if (privatePaths.includes(path)) {
                return <Route {...rest} render={props => (
                    <LoggedInPage>
                        <Component {...props} />
                    </LoggedInPage>
                )}/>
            } else {
                return <Redirect to={homePath}/>;
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        loggedIn: getLoggedIn(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        checkSession: () => dispatch(checkSession())
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppRoute);