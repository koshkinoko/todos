import React from 'react';
import {connect} from 'react-redux';
import {closeModal} from '../actions/error-action';
import {getError} from '../selectors/error-selectors';

const style={position: "fixed", top: "70%", left: "20%", width: "60%", border: "solid gray 1px"};

const ErrorModal = (props) =>
    <div>
        {
            props.error && props.error.showModal &&
            <div className="errorModal" style={style}>
                <p>{props.error.message}</p>
                <button onClick={props.close}>Close</button>
            </div>
        }
    </div>;


const mapStateToProps = (state) => {
    return {
        error: getError(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        close: () => dispatch(closeModal()),
    }
};

export default connect(
    mapStateToProps, mapDispatchToProps
)(ErrorModal);