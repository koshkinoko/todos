import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getUserList} from '../actions/users-actions';
import {getUsers} from '../selectors/userlist-selectors';



const User = (props) => {
    const {user} = props;
    return (
        <div style={{border: "1px solid yellow", backgroundColor: "#ffffe0"}}>
            <div>
                <p>Username: {user.name}</p>
                <p>Role: {user.role}</p>
            </div>
        </div>
    )
};

class Users extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getUsers();
    }

    render() {
        const {users} = this.props;
        return (
            <div className="userlist">
                {users.map(user => {
                    return (
                        <User user={user}
                              key={user.name}/>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: getUsers(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUsers: () => dispatch(getUserList()),
    }
};

export default connect(
    mapStateToProps, mapDispatchToProps
)(Users);