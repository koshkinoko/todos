import React from 'react';
import {Switch} from 'react-router';
import {Redirect, Router} from 'react-router-dom';
import {history} from '../helpers/history';
import {homePath, loginPath, todosPath, usersPath} from '../route-consts';
import HomePage from './HomePage';
import Login from './Login';
import AppRoute from './routes/AppRoute';
import Todos from './Todos';
import Users from './Users';

function App() {
    return (
        <div>
            <Router history={history}>
                <Switch>
                    <AppRoute path={loginPath} component={Login}/>
                    <AppRoute exact path={homePath} component={HomePage}/>
                    <AppRoute path={todosPath} component={Todos}/>
                    <AppRoute path={usersPath} component={Users}/>
                    <Redirect from="*" to={homePath}/>
                </Switch>
            </Router>
        </div>
    );
}

export default App;