import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {deleteTodo, updateTodo} from '../actions/todos-actions';
import {required} from '../helpers/error-helper';
import {isSuperuser} from '../selectors/user-selectors';
import {getRole} from '../selectors/user-selectors';


const validate = values => {
    const errors = {};
    errors.title = required(values.title);
    errors.description = required(values.description);
    return errors;
};

const renderField = ({input, label, type, ComponentType, disabled, meta: {touched, error, warning}}) => (
    <div>
        <label>{label}</label>
        <div>
            <ComponentType {...input} placeholder={label} type={type} disabled={disabled} />
            {touched && ((error && <span style={{color: "red"}}>{error}</span>))}
        </div>
    </div>
);

const TodoForm = props => {
    const {handleSubmit, isEditing, valid} = props;
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Field component={renderField} ComponentType="input" type="text" name="title" label="Title"
                       disabled={!isEditing}/>
            </div>
            <div>
                <Field component={renderField} ComponentType="textarea"
                       type="description" name="description" label="Description"
                       placeholder="Description" disabled={!isEditing}/>
            </div>
            <div>
                <Field component={renderField} ComponentType="input"  type="text" name="createdBy"
                       label="Author" disabled/>
            </div>
            <button type="submit" style={{display: isEditing ? 'inline' : 'none'}} disabled={!valid}>Save</button>
        </form>
    )
};
const TodoFormHoc = reduxForm({
    form: 'todo',
    validate
})(TodoForm);

class Todo extends Component {

    constructor(props) {
        super(props);

        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleSaveChangesClick = this.handleSaveChangesClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.manageButtonsDisplay = this.manageButtonsDisplay.bind(this);
        this.state = {
            isEditable: this.props.isSuperuser || this.props.role === props.todo.createdBy
        };
    }

    manageButtonsDisplay() {
        return this.state.isEditable && !this.props.isEditing ? 'inline' : 'none';
    }

    handleEditClick(e) {
        e.preventDefault();
        this.props.handleEditClick(this.props.todo.id);
    }

    handleDeleteClick(e) {
        e.preventDefault();
        this.props.deleteTodo(this.props.todo.id);
    }

    handleSaveChangesClick(values) {
        this.props.handleSaveChangesClick(values);
    }

    render() {
        const {todo, isEditing, handleCancelClick} = this.props;

        return (
            <div className="todo" style={{border: "1px solid yellow", backgroundColor: "#ffffe0"}}>
                <button onClick={this.handleEditClick} style={{display: this.manageButtonsDisplay()}}>
                    Edit
                </button>
                <button onClick={this.handleDeleteClick} style={{display: this.manageButtonsDisplay()}}>
                    Delete
                </button>
                <TodoFormHoc onSubmit={this.handleSaveChangesClick} form={`TodoForm_${todo.id}`} initialValues={todo}
                             isEditing={isEditing}/>
                <button style={{display: isEditing ? 'inline' : 'none'}} onClick={handleCancelClick}>Cancel
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isSuperuser: isSuperuser(state),
        role: getRole(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteTodo: (id) => dispatch(deleteTodo({id}))
    }
};

export default connect(
    mapStateToProps, mapDispatchToProps
)(Todo);