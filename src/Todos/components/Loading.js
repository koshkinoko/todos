import React from 'react';

export const Loading = () =>
    <div>
        <p>Please wait...</p>
    </div>;