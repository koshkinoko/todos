import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router';
import {Field, reduxForm, startAsyncValidation, startSubmit} from 'redux-form';
import {login} from '../actions/login-actions';
import {STATUS_FETCH_SUCCESS, STATUS_FETCHING} from '../consts';
import {getLoginStatus} from '../selectors/user-selectors';
import {required} from '../helpers/error-helper';


const validate = values => {
    const errors = {};
    errors.login = required(values.login);
    errors.password = required(values.password);
    return errors;
};

const renderField = ({input, label, type, meta: {touched, error, warning}}) => (
    <div>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type}/>
            {touched && ((error && <span style={{color: "red"}}>{error}</span>))}
        </div>
    </div>
)

const LoginForm = props => {
    const {handleSubmit, submitting} = props;
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Field type="text" name="login" label="Login" component={renderField} disabled={submitting}/>
            </div>
            <div>
                <Field type="password" name="password" label="Password" component={renderField} disabled={submitting}/>
            </div>
            <button type="submit">Login</button>
        </form>
    )
};

const LoginFormHoc = reduxForm({form: 'login', validate})(LoginForm);

class Login extends Component {

    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(form) {
        console.log(form);
        return new Promise((resolve, reject) => {
            this.props.submitForm(form, resolve, reject);
        });
    };

    render() {
        const {status} = this.props;
        if(status === STATUS_FETCH_SUCCESS) {
            return <Redirect to='/'/>;
        }
        return (
            <div className="container">
                <LoginFormHoc onSubmit={this.handleSubmit} submitting={status === STATUS_FETCHING}/>
                {status === STATUS_FETCHING && <p>Подождите...</p>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        status: getLoginStatus(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        submitForm: (credentials, resolve, reject) => dispatch(login(credentials, {resolve, reject}))
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);