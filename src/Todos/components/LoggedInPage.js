import React from 'react';
import Navbar from './Navbar';

export const LoggedInPage = props => (
    <div>
        <Navbar/>
        {props.children}
    </div>
)