import {watchCreateTodo, watchDeleteTodo, watchGetTodos, watchUpdateTodo} from './todos-saga';
import {watchCheckSession, watchLogin, watchLogout} from './user-saga';
import {watchGetUsers} from './users-saga';

export function runSagas(sagaMiddleware) {
    sagaMiddleware.run(watchLogin);
    sagaMiddleware.run(watchLogout);
    sagaMiddleware.run(watchCheckSession);
    sagaMiddleware.run(watchGetTodos);
    sagaMiddleware.run(watchUpdateTodo);
    sagaMiddleware.run(watchCreateTodo);
    sagaMiddleware.run(watchDeleteTodo);
    sagaMiddleware.run(watchGetUsers);
}