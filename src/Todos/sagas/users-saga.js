import {put, take, takeEvery} from 'redux-saga/effects';
import {USERS} from '../actions/action-types';
import {FETCH} from '../actions/action-postfixes';
import {getUserListError, getUserListSuccess} from '../actions/users-actions';
import {doGet} from '../helpers/fetch';

export function* watchGetUsers() {
    yield takeEvery(USERS + FETCH, fetchUsers);
}

function* fetchUsers() {
    let res = yield doGet('users');
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(getUserListError(res.error));
    } else {
        yield put(getUserListSuccess(res));
    }
}