import {put, take, takeEvery} from 'redux-saga/effects';
import {CHECK_SESSION, LOGIN, LOGOUT} from '../actions/action-types';
import {FETCH} from '../actions/action-postfixes';
import {
    checkSessionError,
    checkSessionSuccess,
    loginError,
    loginSuccess,
    logoutError,
    logoutSuccess
} from '../actions/login-actions';
import {doGet, doPost} from '../helpers/fetch';

export function* watchLogin() {
    yield takeEvery(LOGIN + FETCH, fetchLogin);
}

function* fetchLogin(action) {
    const {payload} = action;
    let res = yield doPost(
        'login',
        JSON.stringify({
            login: payload.login,
            password: payload.password
        }),);
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(loginError(res.error));
        action.meta.reject();
    } else {
        yield put(loginSuccess(res));
        action.meta.resolve();
    }
}

export function* watchLogout() {
    yield takeEvery(LOGOUT + FETCH, fetchLogout);
}

function* fetchLogout() {
    let res = yield doPost('logout');
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(logoutError(res.error));
    } else {
        yield put(logoutSuccess(res));
    }
}

export function* watchCheckSession() {
    yield takeEvery(CHECK_SESSION + FETCH, fetchCheckSession);
}

function* fetchCheckSession() {
    let res = yield doGet('me');
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(checkSessionError(res.error));
    } else {
        yield put(checkSessionSuccess(res));
    }
}