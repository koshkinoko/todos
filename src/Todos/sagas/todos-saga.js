import {put, take, takeEvery} from 'redux-saga/effects';
import {CREATE_TODO, DELETE_TODO, TODOLIST, UPDATE_TODO} from '../actions/action-types';
import {FETCH} from '../actions/action-postfixes';
import {
    createTodoError,
    createTodoSuccess,
    deleteTodoError,
    deleteTodoSuccess,
    getTodolist,
    getTodolistError,
    getTodolistSuccess,
    updateTodoError,
    updateTodoSuccess
} from '../actions/todos-actions';
import {doDelete, doGet, doPost, doPut} from '../helpers/fetch';

export function* watchGetTodos() {
    yield takeEvery(TODOLIST + FETCH, fetchTodos);
}

function* fetchTodos() {
    let res = yield doGet('todos');
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(getTodolistError(res.error));
    } else {
        yield put(getTodolistSuccess(res));
    }
}

export function* watchCreateTodo() {
    yield takeEvery(CREATE_TODO + FETCH, fetchCreateTodo);
}

function* fetchCreateTodo(action) {
    const {payload} = action;
    let res = yield doPost(
        'todos',
        JSON.stringify({title: payload.title, description: payload.description})
    );
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(createTodoError(res.error));
    } else {
        yield put(createTodoSuccess(res));
    }
}

export function* watchUpdateTodo() {
    yield takeEvery(UPDATE_TODO + FETCH, fetchUpdateTodo);
}

function* fetchUpdateTodo(action) {
    const {payload} = action;
    let res = yield doPut(
        'todos',
        payload.id,
        JSON.stringify({title: payload.title, description: payload.description})
    );
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(updateTodoError(res.error));
    } else {
        yield put(updateTodoSuccess());
        yield put(getTodolist());
    }
}

export function* watchDeleteTodo() {
    yield takeEvery(DELETE_TODO + FETCH, fetchDeleteTodo);
}

function* fetchDeleteTodo(action) {
    const {payload} = action;
    let res = yield doDelete(
        'todos',
        payload.id);
    if (res.isError) {
        if (res.error.action) {
            yield put(res.error.action);
        }
        yield put(deleteTodoError(res.error));
    } else {
        yield put(deleteTodoSuccess());
        yield put(getTodolist());
    }
}