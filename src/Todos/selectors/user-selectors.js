export const getLoginStatus = state => {
    return state.user.status;
};

export const getLoggedIn = state => {
    return state.user.loggedIn;
};

export const getUsername = (state) => {
    return state.user.name;
};

export const getRole = (state) => {
    return state.user.role;
};

export function isSuperuser(state) {
    if(!state.user.role){
        return null;
    }
    return state.user.role === 'admin';
}