import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {applyMiddleware, createStore} from 'redux'
import createSagaMiddleware from 'redux-saga';
import './index.css';
import * as serviceWorker from './serviceWorker';
import App from './Todos/components/App';
import ErrorModal from './Todos/components/ErrorModal';
import {rootReducer} from './Todos/reducers';
import {runSagas} from './Todos/sagas';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
);

runSagas(sagaMiddleware);

ReactDOM.render(
    <Provider store={store}>
        <App/>
        <ErrorModal/>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
